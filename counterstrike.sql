

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `counterstrike`
--

-- --------------------------------------------------------

--
-- Table structure for table `hittable`
--

CREATE TABLE `hittable` (
  `ID` int(11) NOT NULL,
  `steamid` varchar(128) DEFAULT NULL,
  `hit_generic` int(11) DEFAULT '0',
  `hit_head` int(11) DEFAULT '0',
  `hit_chest` int(11) DEFAULT '0',
  `hit_stomach` int(11) DEFAULT '0',
  `hit_leftarm` int(11) DEFAULT '0',
  `hit_rightarm` int(11) DEFAULT '0',
  `hit_leftleg` int(11) DEFAULT '0',
  `hit_rightleg` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hittable`
--


-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE `points` (
  `uid` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `steamid` varchar(128) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '2000',
  `kills` int(11) NOT NULL DEFAULT '0',
  `deaths` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `points`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hittable`
--
ALTER TABLE `hittable`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `steamid` (`steamid`);

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `steamid` (`steamid`);


--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hittable`
--
ALTER TABLE `hittable`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;


-- Constraints for dumped tables
--

--
-- Constraints for table `hittable`
--
ALTER TABLE `hittable`
  ADD CONSTRAINT `hittable_ibfk_1` FOREIGN KEY (`steamid`) REFERENCES `points` (`steamid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
