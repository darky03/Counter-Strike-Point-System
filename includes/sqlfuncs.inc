#if defined __SQL_FUNCS_INCLUDED__
    #endinput
#endif

#define __SQL_FUNCS_INCLUDED__
/*
    SQL Functions
*/
public CheckPlayer(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
    
    CheckSQLError(FailState,Errcode,Error);
    new id = Data[0];
    new query[256];
    if(SQL_NumResults(Query) > 0)
    {
        new f_points = SQL_FieldNameToNum(Query,"points");
        new f_kills = SQL_FieldNameToNum(Query,"kills");
        new f_deaths = SQL_FieldNameToNum(Query,"deaths");
        pData[id][Points] = SQL_ReadResult(Query,f_points);
        pData[id][Kills] = SQL_ReadResult(Query,f_kills);
        pData[id][Deaths] = SQL_ReadResult(Query,f_deaths);
        pData[id][DataLoaded] = true;
        format(query,sizeof(query),"[POINTS]: Loaded your data with: (Points %d) (Kills %d) (Deaths %d)",pData[id][Points],pData[id][Kills],pData[id][Deaths]);
        ColorChat(id,GREEN,query);
    }
    else
    {
        pData[id][Points] = 2000;
        pData[id][Kills] = 0;
        pData[id][Deaths] = 0;
        for(new i = 0; i < 8; i++)
            pData[id][HitData][i] = 0;
        pData[id][DataLoaded] = true;
        client_print(id,print_chat,"Your record does not exist!");
        format(query,sizeof(query),"INSERT INTO points(`username`,`steamid`) VALUES('%s','%s')",GetPlayerName(id),GetPlayerSteamID(id));
        SQL_ThreadQuery(tuple,"EmptyQuery",query);
        format(query,sizeof(query),"INSERT INTO hittable(`steamid`) VALUES('%s')",GetPlayerSteamID(id));
        SQL_ThreadQuery(tuple,"EmptyQuery",query);
    }
}

public EmptyQuery(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
    CheckSQLError(FailState,Errcode,Error);
}

public CheckSQLError(FailState,Errcode,Error[])
{
    if(FailState == TQUERY_CONNECT_FAILED)
        return set_fail_state("Could not connect to SQL database.")
    else if(FailState == TQUERY_QUERY_FAILED)
        return set_fail_state("Query failed.")
   
    if(Errcode)
        return log_amx("Error on query: %s",Error)
    
}
