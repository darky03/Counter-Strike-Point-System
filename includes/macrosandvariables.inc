#if defined __MACROS_AND_VARS_INCLUDED__
    #endinput
#endif 
#define __MACROS_AND_VARS_INCLUDED__

#define     HOST        ""
#define     USER        ""
#define     PASS        ""
#define     DB          ""
#define     MIN_MAX_PLAYERS     32

new Handle:tuple;
new sqlError[512];

enum PlayerData
{
    bool:DataLoaded,
    Points,
    Kills,
    Deaths,
    HitData[8]
}
new pData[MIN_MAX_PLAYERS][PlayerData];
new hittablelist[8][] =
{
    {"hit_generic"},
    {"hit_head"},
    {"hit_chest"},
    {"hit_stomach"},
    {"hit_leftarm"},
    {"hit_rightarm"},
    {"hit_leftleg"},
    {"hit_rightleg"}
};


new PointGainString[512];