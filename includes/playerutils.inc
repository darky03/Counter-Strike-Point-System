#if defined __PLAYER_UTILS_INCLUDED__
    #endinput
#endif 
#define __PLAYER_UTILS_INCLUDED__

public GetPlayerSteamID(id)
{
    new SteamID[128];
    get_user_authid(id,SteamID,sizeof(SteamID));
    return SteamID;
}
public GetPlayerName(id)
{
    new PlayerName[128];
    get_user_name(id,PlayerName,sizeof(PlayerName));
    return PlayerName;
}