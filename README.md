# Counter-Strike-Point-System

Point system made in PAWN language using MySQL for Counter strike

# Installation

## Compiling the source
For compiling the source you need to put the source files in the appropriate location

`point.sma` -> `scripting folder`

`includes/macrosandvariables.inc` -> `scripting/includes folder`

`includes/playerutils.inc` -> `scripting/includes folder`

`includes/pointcommands.inc` -> `scripting/includes folder`

`includes/sqlfuncs.inc` -> `scripting/includes folder`

Now you can run the *compile.exe* for compiling either all the `.sma` scripts present in your scripting folder

or you may run the following command using a command prompt for a specific script:

(You have to first be present in the scripting folder to run this command in the command prompt , Make sure you have something like *C:\xyz\csfolder\cstrike\addons\amxmodx\scripting>*)

Command: `compile.exe psys.sma`

After it compiles sucessfully without any errors (Warnings might be present but they can be ignored)

You have to move the `psys.amxx` file present in the `scripting/compiled` to `amxmodx/plugins` folder.

Also after doing this add one more line in the `plugins.ini` present in `configs` folder

`psys.amxx`

## Database configuration and inscript database configuration

For connecting the script to **Mysql**, we have to first configure the script's includes which contain the macro defination present for `HOST`,`PASS`,`USER`,`DB`

To do so we have to edit the `macrosandvariables.inc`.

Enter the host ip,database username,database user password,database in quotes given in the include.

For configuring the actual mysql database , download the `counterstrike.sql` file and import it in your database

