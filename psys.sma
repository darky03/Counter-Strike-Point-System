#include <amxmodx>
#include <sqlx>
#include <colorchat>
/*Custom includes*/
#include <macrosandvariables>
#include <playerutils>
#include <pointcommands>
#include <sqlfuncs>

public plugin_init()
{
    console_print(0,"Loading point system..")
    //register_logevent("SavePoints", 2, "1=Round_End")  
    register_event("HLTV", "SavePoints", "a", "1=0", "2=0")  
    register_clcmd("say /points", "cmdPoints")
    tuple = SQL_MakeDbTuple(HOST,USER,PASS,DB);    
}

public SavePoints()
{
    new savestring[512];
    new k = 0;
    for(new i = 0; i < MIN_MAX_PLAYERS; i++)
    {
        if(is_user_connected(i))
        {
            if(pData[i][DataLoaded] == true)
            {
                format(savestring,sizeof(savestring),"UPDATE `points` SET `points` = %d,`kills`= %d,`deaths` = %d WHERE `steamid` = '%s'",pData[i][Points],pData[i][Kills],pData[i][Deaths],GetPlayerSteamID(i));
                //client_print(i,print_chat,savestring);
                SQL_ThreadQuery(tuple,"EmptyQuery",savestring);
                format(savestring,sizeof(savestring),"UPDATE `hittable` SET ");
                
                for(k = 0; k < 7; k++)
                {
                    format(savestring,sizeof(savestring),"%s`%s` = %d,",savestring,hittablelist[k],pData[i][HitData][k]);
                }
                format(savestring,sizeof(savestring),"%s`%s` = %d WHERE `steamid`= '%s'",savestring,hittablelist[k],pData[i][HitData][k],GetPlayerSteamID(i));
                SQL_ThreadQuery(tuple,"EmptyQuery",savestring);
            }
        }
    }
}
public client_putinserver(id)
{
    pData[id][DataLoaded] = false;
    new taskarray[1];
    taskarray[0] = id;
    set_task(5.0,"LoadPlayer",0,taskarray,sizeof(taskarray));
}

public client_death(killer,victim,wpnindex,hitplace,TK)
{
    switch(wpnindex)
    {
        case 29:
        {
            pData[killer][Points] += 4;
            pData[victim][Points] -= 1;
            format(PointGainString,sizeof(PointGainString),"[POINTS]: ^x3%s [%d] (+4) ^x4gains 2 points for killing ^x3%s [%d] (-1) with a knife",GetPlayerName(killer),pData[killer][Points],GetPlayerName(victim),pData[victim][Points]);
        }
        default:
        {
            pData[killer][Points] += 2;
            pData[victim][Points] -= 1;
            format(PointGainString,sizeof(PointGainString),"[POINTS]: ^x3%s [%d] (+2) ^x4gains 2 points for killing ^x3%s [%d] (-1)",GetPlayerName(killer),pData[killer][Points],GetPlayerName(victim),pData[victim][Points]);
        }
    }
    pData[killer][HitData][hitplace]++;
    pData[killer][Kills]++;
    pData[victim][Deaths]++;
    ColorChat(0,GREEN,PointGainString);
    format(PointGainString,sizeof(PointGainString),"HitPlace: %d",hitplace);
    client_print(killer,print_chat,PointGainString);

}
public LoadPlayer(array[],taskid)
{
    new id = array[0];
    new string[256];
    format(string,sizeof(string),"Your id: %d",id);
    client_print(id,print_chat,string);
    new sqldataarray[1];
    sqldataarray[0] = id;
    format(string,sizeof(string),"SELECT * FROM `points` WHERE `steamid` = '%s'",GetPlayerSteamID(id));
    SQL_ThreadQuery(tuple,"CheckPlayer",string,sqldataarray,sizeof(sqldataarray));
}

